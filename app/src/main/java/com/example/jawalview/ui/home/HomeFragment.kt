package com.example.jawalview.ui.home

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.jawalview.R
import com.example.jawalview.adapter.MainAdapter
import com.example.jawalview.delegates.MainAdapterDelegate
import com.example.jawalview.utils.Utils
import com.google.android.material.bottomsheet.BottomSheetBehavior
import kotlinx.android.synthetic.main.fragment_home.view.*


 class HomeFragment : Fragment(), MainAdapterDelegate{

    private lateinit var homeViewModel: HomeViewModel
    private var recyclerView: RecyclerView? = null
    private var gridLayoutManager: GridLayoutManager? = null
    private var mainAdapter: MainAdapter? = null
    var bottomSheet: View? = null
    var bottomSheetBehavior: BottomSheetBehavior<View?>? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_home, container, false)
        if(Utils.homePageFirstTime == 0)
            Utils.readMainPageData()
        recyclerView = root.recyclerView
        gridLayoutManager = GridLayoutManager(context, 3, LinearLayoutManager.VERTICAL, false)
        recyclerView?.layoutManager = gridLayoutManager
        recyclerView?.setHasFixedSize(true)
        mainAdapter = MainAdapter(requireContext(), Utils.homePageItems)
        mainAdapter?.mainAdapterDelegate = this
        recyclerView?.adapter = mainAdapter

        bottomSheet = root.mainBottomSheet
        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet!!)
        return root
    }

     override fun showBottomSheet() {
         bottomSheetBehavior?.state = BottomSheetBehavior.STATE_EXPANDED
     }
 }