package com.example.jawalview.dataholder

data class DataSource (var id: Int?, var label: String?, var imagePath: Int?, var type: ItemType?, var secondLabel: String? = null)
