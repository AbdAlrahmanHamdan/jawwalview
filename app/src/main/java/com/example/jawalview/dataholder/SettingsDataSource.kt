package com.example.jawalview.dataholder

data class SettingsDataSource (var id: Int?, var label: String?, var imagePath: Int?, var checked: Boolean = true)