package com.example.jawalview.dataholder

enum class ItemType {
    Item, NavigationItem, HeaderNavigationItem, LogoutNavigationItem
}