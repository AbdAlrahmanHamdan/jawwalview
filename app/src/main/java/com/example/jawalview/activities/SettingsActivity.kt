package com.example.jawalview.activities

import android.app.Activity
import android.content.Intent
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.children
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.jawalview.R
import com.example.jawalview.adapter.SettingsAdapter
import com.example.jawalview.dataholder.DataSource
import com.example.jawalview.dataholder.ItemType
import com.example.jawalview.ui.home.HomeFragment
import com.example.jawalview.utils.Utils
import kotlinx.android.synthetic.main.item_settings.view.*
import kotlinx.android.synthetic.main.layout_settings.*

class SettingsActivity : AppCompatActivity(), View.OnClickListener {

    private var recyclerView: RecyclerView? = null
    private var gridLayoutManager: GridLayoutManager? = null
    private var settingsAdapter: SettingsAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_settings)
        if (Utils.settingsPageFirstTime == 0)
            Utils.readSettingItemData()
        recyclerView = settingsRecyclerView
        gridLayoutManager = GridLayoutManager(this@SettingsActivity, 3, LinearLayoutManager.VERTICAL, false)
        recyclerView?.layoutManager = gridLayoutManager
        settingsAdapter = SettingsAdapter(this@SettingsActivity, Utils.settingsItems)
        recyclerView?.adapter = settingsAdapter

        AddButton.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        if (p0?.id == AddButton.id) {
            Utils.homePageItems.clear()

            for (itemHolder in recyclerView!!.children){
                if (((itemHolder.settingItemStatus.drawable) as BitmapDrawable).bitmap == ((ContextCompat.getDrawable(this@SettingsActivity, R.drawable.checked) as BitmapDrawable).bitmap)) {
                     Utils.homePageItems.add(DataSource(itemHolder.settingItemLabel.tag as Int, itemHolder.settingItemLabel.text.toString(), itemHolder.settingItemImage.tag as Int, ItemType.Item ))
                     Utils.settingsItems[(itemHolder.settingItemLabel.tag as Int) - 1].checked = true
                }
                else {
                    Utils.settingsItems[(itemHolder.settingItemLabel.tag as Int) - 1].checked = false
                }
            }
            Utils.homePageItems.add(DataSource(16, "أضف", R.drawable.add_new, ItemType.Item))
            val intent = Intent(this@SettingsActivity, HomeFragment::class.java)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }
    }
}