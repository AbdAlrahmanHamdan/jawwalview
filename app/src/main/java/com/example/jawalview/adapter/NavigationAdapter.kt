package com.example.jawalview.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.example.jawalview.R
import com.example.jawalview.dataholder.DataSource
import com.example.jawalview.dataholder.ItemType
import kotlinx.android.synthetic.main.item_container.view.*

class NavigationAdapter(var context: Context, var arrayList: ArrayList<DataSource>) : RecyclerView.Adapter<NavigationAdapter.ItemHolder>() {

    override fun getItemViewType(position: Int): Int {
        if (arrayList[position].type == ItemType.NavigationItem)
            return 1
        else if (arrayList[position].type == ItemType.HeaderNavigationItem)
            return 2
        else
            return 3
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder {
        var itemHolder: View? = null
        if (viewType == 1)
            itemHolder = LayoutInflater.from(parent.context).inflate(R.layout.item_navigation, parent, false)
        else if (viewType == 2)
            itemHolder = LayoutInflater.from(parent.context).inflate(R.layout.item_navigation_header, parent, false)
        else {
            itemHolder = LayoutInflater.from(parent.context).inflate(R.layout.item_navigation, parent, false)
            itemHolder.findViewById<ImageView>(R.id.arrowImage).visibility = View.INVISIBLE
        }
        return ItemHolder(itemHolder!!)
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(holder: ItemHolder, position: Int) {
        val item: DataSource = arrayList[position]
        holder.image.setImageResource(item.imagePath!!)
        holder.label.text = item.label
    }

    class ItemHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {
        val label = itemView.itemName
        val image = itemView.itemImage
    }
}