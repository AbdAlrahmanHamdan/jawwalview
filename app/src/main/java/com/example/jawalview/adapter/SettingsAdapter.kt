package com.example.jawalview.adapter

import android.content.Context
import android.graphics.drawable.BitmapDrawable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat.getDrawable
import androidx.recyclerview.widget.RecyclerView
import com.example.jawalview.R
import com.example.jawalview.dataholder.SettingsDataSource
import kotlinx.android.synthetic.main.item_settings.view.*

class SettingsAdapter (var context: Context, var arrayList: ArrayList<SettingsDataSource>) : RecyclerView.Adapter<SettingsAdapter.ItemHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder {
        val itemHolder = LayoutInflater.from(parent.context).inflate(R.layout.item_settings, parent, false)

        if (viewType == 1)
            itemHolder.settingItemStatus.setImageResource(R.drawable.checked)

        else
            itemHolder.settingItemStatus.setImageResource(R.drawable.not_checked)

        itemHolder.setOnClickListener{
            if (((itemHolder.settingItemStatus.drawable) as BitmapDrawable).bitmap == ((getDrawable(context, R.drawable.not_checked) as BitmapDrawable).bitmap))
                itemHolder.settingItemStatus.setImageResource(R.drawable.checked)
            else
                itemHolder.settingItemStatus.setImageResource(R.drawable.not_checked)
        }
        return ItemHolder(itemHolder)
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun getItemViewType(position: Int): Int {
        if (arrayList[position].checked)
            return 1
        else
            return 2
    }

    override fun onBindViewHolder(holder: ItemHolder, position: Int) {
        val item: SettingsDataSource = arrayList[position]
        holder.image.setImageResource(item.imagePath!!)
        holder.image.tag = item.imagePath!!
        holder.label.text = item.label
        holder.label.tag = item.id
    }

    class ItemHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {
        val label = itemView.settingItemLabel
        val image = itemView.settingItemImage
    }
}