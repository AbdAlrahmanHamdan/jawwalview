package com.example.jawalview.adapter

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.jawalview.R
import com.example.jawalview.activities.SettingsActivity
import com.example.jawalview.dataholder.DataSource
import com.example.jawalview.dataholder.ItemType
import com.example.jawalview.delegates.MainAdapterDelegate
import kotlinx.android.synthetic.main.item_container.view.*


class MainAdapter(var context: Context, var arrayList: ArrayList<DataSource>) : RecyclerView.Adapter<MainAdapter.ItemHolder>() {
    var mainAdapterDelegate: MainAdapterDelegate? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder {
        val itemHolder = LayoutInflater.from(parent.context).inflate(R.layout.item_container, parent, false)
        return ItemHolder(itemHolder)
    }

    override fun getItemViewType(position: Int): Int {
        if (arrayList[position].type == ItemType.Item)
            return 1
        else
            return 2
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(holder: ItemHolder, position: Int) {
        val item: DataSource = arrayList[position]
        holder.image.setImageResource(item.imagePath!!)
        holder.label.text = item.label
        if (item.id == 1)
            holder.image.setOnClickListener{
                val intent = Intent(context, SettingsActivity::class.java)
                (context as Activity).startActivityForResult(intent, 1)
            }
        else
            holder.image.setOnClickListener{
                mainAdapterDelegate?.showBottomSheet()
            }
    }

    class ItemHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {
        val label = itemView.itemName
        val image = itemView.itemImage
    }

}