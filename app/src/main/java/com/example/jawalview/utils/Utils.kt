package com.example.jawalview.utils

import com.example.jawalview.R
import com.example.jawalview.dataholder.DataSource
import com.example.jawalview.dataholder.ItemType
import com.example.jawalview.dataholder.SettingsDataSource

class Utils {
    companion object {
        val homePageItems: ArrayList<DataSource> = ArrayList()
        val navigationItems: ArrayList <DataSource> = ArrayList()
        val settingsItems: ArrayList<SettingsDataSource> = ArrayList()
        var homePageFirstTime = 0
        var settingsPageFirstTime = 0
        fun readMainPageData () {
            homePageItems.add(DataSource(1, "الاعدادات", R.drawable.settings, ItemType.Item))
            homePageItems.add(DataSource(2, "الشرائح", R.drawable.sims, ItemType.Item))
            homePageItems.add(DataSource(3, "الكودات المختصرة", R.drawable.pormo_codes, ItemType.Item))
            homePageItems.add(DataSource(4, "الصيانة", R.drawable.maintenance, ItemType.Item))
            homePageItems.add(DataSource(5, "التحصيلات", R.drawable.collections, ItemType.Item))
            homePageItems.add(DataSource(6, "دائرة الموزعين", R.drawable.distributor_service, ItemType.Item))
            homePageItems.add(DataSource(16, "أضف", R.drawable.add_new, ItemType.Item))
            homePageItems.add(DataSource(8, "الخدمات", R.drawable.services, ItemType.Item))
            homePageItems.add(DataSource(9, "تواصل معنا", R.drawable.contact_with_us, ItemType.Item))
            homePageFirstTime++
        }

        fun readNavigationItemsData () {
            navigationItems.add(DataSource(1, "Salam Ahmad", R.drawable.profile_picture, ItemType.HeaderNavigationItem, "Salam Ahmad"))
            navigationItems.add(DataSource(2, "الصفحة الرئيسية", R.drawable.ic_homepage, ItemType.NavigationItem))
            navigationItems.add(DataSource(3, "تغير الرقم السري", R.drawable.ic_change_password, ItemType.NavigationItem))
            navigationItems.add(DataSource(4, "الاعدادات", R.drawable.ic_settings, ItemType.NavigationItem))
            navigationItems.add(DataSource(5, "تسحيل الخروج", R.drawable.ic_logout, ItemType.LogoutNavigationItem))
        }

        fun readSettingItemData(){
            settingsItems.add(SettingsDataSource(1, "الاعدادات", R.drawable.settings))
            settingsItems.add(SettingsDataSource(2, "الشرائح", R.drawable.sims))
            settingsItems.add(SettingsDataSource(3, "عروض", R.drawable.offers, false))
            settingsItems.add(SettingsDataSource(4, "الصيانة", R.drawable.maintenance))
            settingsItems.add(SettingsDataSource(5, "التحصيلات", R.drawable.collections))
            settingsItems.add(SettingsDataSource(6, "دائرة الموزعين", R.drawable.distributor_service))
            settingsItems.add(SettingsDataSource(7, "تواصل اجتماعي", R.drawable.social_media, false))
            settingsItems.add(SettingsDataSource(8, "البضائع الالكترونية", R.drawable.electronic_goods, false))
            settingsItems.add(SettingsDataSource(9, "الخدمات", R.drawable.services))
            settingsItems.add(SettingsDataSource(10, "الخدمات المضافة", R.drawable.added_services, false))
            settingsItems.add(SettingsDataSource(11, "التعميمات", R.drawable.generalizations, false))
            settingsItems.add(SettingsDataSource(12, "الدليل", R.drawable.guide, false))
            settingsItems.add(SettingsDataSource(13, "تواصل معنا", R.drawable.contact_with_us))
            settingsItems.add(SettingsDataSource(14, "التقراير", R.drawable.reports, false))
            settingsItems.add(SettingsDataSource(15, "الكودات المختصرة", R.drawable.pormo_codes))
            settingsPageFirstTime++
        }

    }
}